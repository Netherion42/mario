{
    "id": "34acf559-96f8-44fd-af19-ceb859a6e98f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_recover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b6d61eb-6413-45a9-99b1-1728cacb46a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34acf559-96f8-44fd-af19-ceb859a6e98f",
            "compositeImage": {
                "id": "37a7c279-5758-4d1c-939a-2e281b87c093",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b6d61eb-6413-45a9-99b1-1728cacb46a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd564071-3d02-4961-b8d8-31745d7e5d28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b6d61eb-6413-45a9-99b1-1728cacb46a1",
                    "LayerId": "c1a274cb-ea80-4954-8251-69c2401e5db8"
                }
            ]
        },
        {
            "id": "91d035d4-f0b3-47e0-a260-6c3f4cde9ea8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34acf559-96f8-44fd-af19-ceb859a6e98f",
            "compositeImage": {
                "id": "882b059e-5a06-42b0-9001-b925888735e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d035d4-f0b3-47e0-a260-6c3f4cde9ea8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36aeb8c4-f937-4954-8669-458367c9ba6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d035d4-f0b3-47e0-a260-6c3f4cde9ea8",
                    "LayerId": "c1a274cb-ea80-4954-8251-69c2401e5db8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c1a274cb-ea80-4954-8251-69c2401e5db8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34acf559-96f8-44fd-af19-ceb859a6e98f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 8
}