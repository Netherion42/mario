/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 3C849F7A
/// @DnDArgument : "code" "if(room != Menu)$(13_10){$(13_10)	draw_set_halign(fa_left);																	$(13_10)	draw_set_valign(fa_middle);																	$(13_10)	draw_set_font(fnt_score);																	$(13_10)	draw_set_color(c_white);																	$(13_10)																							$(13_10)	draw_text(room_width/4-60, room_height/4-50, "Score:" + string(global.puntos));			$(13_10)	draw_text(room_width/2-50, room_height/4-50, "Max Score:" + string(global.maxScore));$(13_10)	draw_text(room_width - 70, room_height/4-50, "Lives:" + string(global.vidas));$(13_10)}"
if(room != Menu)
{
	draw_set_halign(fa_left);																	
	draw_set_valign(fa_middle);																	
	draw_set_font(fnt_score);																	
	draw_set_color(c_white);																	
																							
	draw_text(room_width/4-60, room_height/4-50, "Score:" + string(global.puntos));			
	draw_text(room_width/2-50, room_height/4-50, "Max Score:" + string(global.maxScore));
	draw_text(room_width - 70, room_height/4-50, "Lives:" + string(global.vidas));
}