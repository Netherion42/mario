{
    "id": "7034b3af-0b8f-4e95-a251-eb33c842a1cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_turning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e276c39c-9717-4520-a6cb-c8ab5ca0522e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7034b3af-0b8f-4e95-a251-eb33c842a1cb",
            "compositeImage": {
                "id": "b5d02657-996f-4be6-a2d8-822eef7da9dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e276c39c-9717-4520-a6cb-c8ab5ca0522e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5593be20-3624-4990-b4dd-6cf6e8039f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e276c39c-9717-4520-a6cb-c8ab5ca0522e",
                    "LayerId": "65c076ab-dbde-44ce-8a2c-318b29f5352f"
                }
            ]
        },
        {
            "id": "455ceb69-0b63-47ad-8590-cd362c11cf17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7034b3af-0b8f-4e95-a251-eb33c842a1cb",
            "compositeImage": {
                "id": "6dc2e22f-5e0f-41ed-81d8-e9035794cc3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "455ceb69-0b63-47ad-8590-cd362c11cf17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e82521-6f45-47e0-bed9-13564fd2c4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "455ceb69-0b63-47ad-8590-cd362c11cf17",
                    "LayerId": "65c076ab-dbde-44ce-8a2c-318b29f5352f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "65c076ab-dbde-44ce-8a2c-318b29f5352f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7034b3af-0b8f-4e95-a251-eb33c842a1cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}