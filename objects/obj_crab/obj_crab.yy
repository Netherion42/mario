{
    "id": "3df08797-9605-4069-bf9e-f2f20ac5440e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_crab",
    "eventList": [
        {
            "id": "ac99b855-00f4-4d8b-8090-4d7a493ddaf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "d5c62a9f-32c9-462f-a2d0-55a793a6ad36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "68c17eba-f4e0-43d5-a128-01ff524676be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5381da24-7973-4a3e-ad22-506ffc6bfe01",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "4bb4155b-f220-4740-b5dd-7e83b033d19e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "7ba26df9-1f2d-4615-bfca-e8f8023cf568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "659c1f09-9a5a-4b51-81ab-e23aa743c55d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b8f4cd2e-bf90-4806-b816-de6c56071517",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "e1c6ab98-996f-48bb-a6ea-ad46b40ebb63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        },
        {
            "id": "fbe90ce0-cd60-417b-b8e1-2f7cc96156e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3df08797-9605-4069-bf9e-f2f20ac5440e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a228d94b-ce6d-455a-b841-be771e370389",
    "visible": true
}