{
    "id": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10f05ebf-d2a9-46fe-8eb4-6a8c42314d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
            "compositeImage": {
                "id": "8e51f18b-bc70-4093-b774-19e6d36a5fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f05ebf-d2a9-46fe-8eb4-6a8c42314d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10eeb3f0-6844-4c2d-9f48-1b1b8533f070",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f05ebf-d2a9-46fe-8eb4-6a8c42314d96",
                    "LayerId": "cac35507-2a63-465d-b0e8-831188be7918"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "cac35507-2a63-465d-b0e8-831188be7918",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}