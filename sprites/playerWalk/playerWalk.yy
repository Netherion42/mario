{
    "id": "423cd21e-0007-4237-ae75-582c4ed3cfff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aea3cc7b-e355-428f-873e-7a595345e7d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423cd21e-0007-4237-ae75-582c4ed3cfff",
            "compositeImage": {
                "id": "c0e44606-269f-43ea-a657-5557f884f13d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aea3cc7b-e355-428f-873e-7a595345e7d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f88dbd-7aa2-4bdd-96e7-e78a9bbe1991",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aea3cc7b-e355-428f-873e-7a595345e7d6",
                    "LayerId": "a6d4da58-597f-4b6b-9951-5c9e13130d61"
                }
            ]
        },
        {
            "id": "4d4ecd3b-9baf-4cae-9ad0-5f4e63debbde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423cd21e-0007-4237-ae75-582c4ed3cfff",
            "compositeImage": {
                "id": "1932719e-f104-4588-8d80-62dd18a3ad00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d4ecd3b-9baf-4cae-9ad0-5f4e63debbde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db72d97d-f887-4ffb-b1ce-837ac57c0cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d4ecd3b-9baf-4cae-9ad0-5f4e63debbde",
                    "LayerId": "a6d4da58-597f-4b6b-9951-5c9e13130d61"
                }
            ]
        },
        {
            "id": "abeec724-4988-4811-9760-7fe330774dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423cd21e-0007-4237-ae75-582c4ed3cfff",
            "compositeImage": {
                "id": "a40b9514-8670-4a9a-9a3e-f3da4736c904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abeec724-4988-4811-9760-7fe330774dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43412a2e-c82e-43a3-99a4-4339dd3486df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abeec724-4988-4811-9760-7fe330774dc0",
                    "LayerId": "a6d4da58-597f-4b6b-9951-5c9e13130d61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "a6d4da58-597f-4b6b-9951-5c9e13130d61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "423cd21e-0007-4237-ae75-582c4ed3cfff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 11
}