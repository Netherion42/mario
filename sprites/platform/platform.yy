{
    "id": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "platform",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a432471-ffe4-4f4f-ae16-8443f2374ba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "compositeImage": {
                "id": "4101987f-a3ef-483d-acc6-f504dd3d4f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a432471-ffe4-4f4f-ae16-8443f2374ba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36ea19e8-8d1a-42d3-9b25-abfc6d4e0885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a432471-ffe4-4f4f-ae16-8443f2374ba9",
                    "LayerId": "0920f7c1-b4c0-460d-863f-b6b6fc46679e"
                }
            ]
        },
        {
            "id": "c81f97ec-2b83-4933-8107-ef249b012caf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "compositeImage": {
                "id": "0e472120-f9ee-4b7b-802a-bcd8eb790ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c81f97ec-2b83-4933-8107-ef249b012caf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d0d091-e1eb-4f78-96e8-cccee9b849da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c81f97ec-2b83-4933-8107-ef249b012caf",
                    "LayerId": "0920f7c1-b4c0-460d-863f-b6b6fc46679e"
                }
            ]
        },
        {
            "id": "8ffbb910-7212-4a50-aa77-bc54285f9229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "compositeImage": {
                "id": "32d691c1-c445-4abe-8c24-42ac9f2f80d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ffbb910-7212-4a50-aa77-bc54285f9229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73e82e7-7c8b-444b-b9f7-537ccae1aa7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ffbb910-7212-4a50-aa77-bc54285f9229",
                    "LayerId": "0920f7c1-b4c0-460d-863f-b6b6fc46679e"
                }
            ]
        },
        {
            "id": "a413f58b-43bb-4704-8b70-b94b7efe96e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "compositeImage": {
                "id": "5073a31f-8e47-4a49-bf2d-347c8422f87c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a413f58b-43bb-4704-8b70-b94b7efe96e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b9fe3a0-e859-40c3-9fb8-c87c60517353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a413f58b-43bb-4704-8b70-b94b7efe96e8",
                    "LayerId": "0920f7c1-b4c0-460d-863f-b6b6fc46679e"
                }
            ]
        },
        {
            "id": "0a746573-bc81-45a4-a39f-d0d492b8cc8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "compositeImage": {
                "id": "f656d885-516d-4e4d-a62b-1259c917befa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a746573-bc81-45a4-a39f-d0d492b8cc8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0537a98b-b207-45b4-a157-a94a6bcefa53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a746573-bc81-45a4-a39f-d0d492b8cc8d",
                    "LayerId": "0920f7c1-b4c0-460d-863f-b6b6fc46679e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "0920f7c1-b4c0-460d-863f-b6b6fc46679e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc8d30d2-a86e-4f80-837e-a3da4e969422",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 7
}