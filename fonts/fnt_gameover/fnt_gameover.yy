{
    "id": "10fa1253-365b-4426-a5ab-87e6465471da",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_gameover",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "dae1664b-4bd3-40c0-8e8a-5f2f1ee9dc09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f6834476-200f-45b2-be42-c32f5c0a8dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 6,
                "x": 116,
                "y": 117
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6133e316-50f6-418b-9d0d-06c7347c0aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 216,
                "y": 94
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cc67b368-6399-455a-8e52-c8cf15098a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 94
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "86f7a05d-f151-467e-9af9-c13c979b72e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 71
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f15452c8-3c82-429a-84b5-dd88a1173cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 71
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f28649a3-3c1a-489c-8ca8-9eeb372468a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0f514eac-9ded-4efe-a686-c42ddab4b636",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 4,
                "x": 150,
                "y": 117
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "8fdbb94b-c591-4482-b90b-f25197a8d233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 60,
                "y": 117
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4d8f1d47-4cee-4986-ae34-e149c3028099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 8,
                "x": 70,
                "y": 117
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "520952be-da88-4f27-b25e-e6c74be69d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 14,
                "y": 117
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9525f4a6-7fb8-49ca-9843-003d24a9e724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ba991520-940b-45cc-abda-ca3ce8bcc5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 108,
                "y": 117
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "33592f94-1932-479d-855e-7f39443e589f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 228,
                "y": 94
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "641013ac-e9a7-4610-99ca-5a76dd7daa9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 132,
                "y": 117
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "53225bdd-9644-4268-aec5-9e06f1066d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 71
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dc743be6-ba6a-4d4c-8a8d-c08fa088cc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2a20b1e5-3e19-4fc9-aa1e-6a6b466cdaa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 124,
                "y": 94
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0a612962-af34-41c7-af3d-a76673a642c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 71
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8bb11989-bd1e-404e-9760-ba92570bfe33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1ed46b15-c183-4fc2-9f1d-d9df592e2901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "145b2e8b-d7cd-4fd3-9872-52972e867199",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "33fcb23d-bdba-472a-8e0e-7bbfd9c11801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dbb45d51-bc73-4435-8490-c5049a82b9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "66413929-3bd9-4f9a-94e7-dbea621a3c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c3d9c8f1-195f-4721-8b91-34dee5aa4c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6d3a881d-7f1e-4db6-82b4-b93744983e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 138,
                "y": 117
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f586ff2a-684b-4446-8528-3c5564461c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 6,
                "x": 100,
                "y": 117
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0a9a9cd3-0665-4ab7-a150-69707167c48f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 38,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "05bff13f-9f47-4bbb-8a0a-c129cb4a7eb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 26,
                "y": 117
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "157e6942-6791-4195-af55-fe12a3436c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 240,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "7809fa8e-6f3a-4715-ad31-57f458472f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fc82bca5-6e0a-4ec1-a2cb-c7a128f4a3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "66bcaf7a-4edb-4801-bc2a-9af29b1d6b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0631ae20-e935-4f85-814b-1cc7cc74cd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bf39dfb0-e147-41ef-8838-eb587a814182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "41b7bce6-84e2-45b0-a6aa-61dfc1ed917d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b490ada2-f9e0-4371-9d15-261338f74a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "58cc936d-9c00-46ab-ae3a-20a4ba193413",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 94
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "da6dafce-e1e3-4c01-bb3c-3e1eacacd4a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 94
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cfbcbf38-b510-4295-9f54-c539698fe260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "65be8ce8-5b7a-443c-b576-355d1386da74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 138,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "293f2db5-721d-4f8b-8c8b-a7fc5717f951",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 48
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f8bcb99a-fefc-481c-bc20-77163696ca9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 48
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "5294a425-2ee8-4d62-bbca-f8f8cbb1a5bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 25
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "647da5fb-5190-4284-a7ba-9a1a4a90be7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4ec8acb6-54d4-44f6-9063-4e7c4fc143ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0f69e4ae-dc41-4611-9e21-a0f68a6c16e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4965912b-c5db-42b3-8d0b-22c17a742dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c99b3108-f12f-4b00-944a-2d08a166407f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c6416575-0d29-4e1f-b6cc-5425cdae93ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "4d2b83d7-2427-4ff8-883f-d15b067396e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "400d7071-22b4-450e-aae9-cb04f8cb21f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 166,
                "y": 94
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ff36d8d9-5af3-43a6-b60b-2400270eb3f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "45414ebc-2d18-4ec4-9e1f-f19a1d3910bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "39702a7f-de0f-4060-9ef1-9cc21f7c8927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4d20f38e-03f4-47d2-85e9-63ea086a93ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "86b5ce5c-b0f3-423d-a742-918f91c194bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 152,
                "y": 94
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a96dcbc0-0648-43d1-949a-276fb56d4e5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c4a0c615-77f0-46b0-8d05-80ebe1ad97e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 8,
                "x": 80,
                "y": 117
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "80be1539-412a-4c50-ba65-3d21727e4003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "aaed5089-16b4-4453-baa1-9d71b512921a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 90,
                "y": 117
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5b9523d9-8674-49b9-b4f9-c8d307459584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 180,
                "y": 94
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "761f1a4d-bc42-4d54-975c-737c6bcbc977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9ef85f2d-d740-4999-9dcf-07ee88235f4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 8,
                "shift": 16,
                "w": 6,
                "x": 124,
                "y": 117
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fa5be10e-53ec-4f5c-87e5-1ef930e90075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f3232477-7a3b-4dec-aa83-a3e659d50538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e5cd3a92-8b9e-4a6b-a15c-a5b78e91c739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e379a11b-1504-4e93-b744-69c9a25164c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 25
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8ce13e31-66c6-4520-9311-572b4e7abed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 25
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4252b74c-c6c2-4d09-a088-a20b8c3767cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "b3da0103-fbea-4b4e-880c-b9a8dc94079a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 48
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d3d773a2-80d8-4af4-b35c-59892ebcd5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "6e28a9e2-5948-485c-8876-794080b2ecd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 96,
                "y": 94
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7f779ca7-7f5a-4178-9f9d-c1807c3de88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e5349a14-4987-4610-9f29-21e697a2fdc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e3ada9d4-bb55-4bf6-b000-1d3bb114f9be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5bffc3ec-03d5-4bef-ac63-7364adc683f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 48
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "23de697a-1f82-4ac8-b874-d11d225543fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7380584a-8de6-46aa-a930-842a28e782f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9096debd-859e-4fcb-875d-674458c997f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c416a797-2b29-441c-9c00-deef1d1cbdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "94bd45b0-a8b6-4a0b-ab37-33b677062311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "14ab3e4f-7053-4a80-a4d5-8891ddeb895d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8bccde52-883a-4ff3-a423-c93bfca3d355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 82,
                "y": 94
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4e27ff8b-6250-4da1-8f38-7ec8920b28fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "be0989c8-ba7b-42a4-a19d-bc0c7092469c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6c43c8d5-23fd-44dd-bbb7-1f3ee224fffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a59d095e-f037-40f4-a6aa-129abd3d82c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "60a391b7-7508-47fd-a4e2-2912f81c15bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 110,
                "y": 94
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cf1bd428-eaea-4219-90e2-3c915764beca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "42a05ae6-ac47-49f9-aed8-b33999c2410e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 192,
                "y": 94
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "842ba4a4-0aa2-4b6b-bfc6-326de4164622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 6,
                "shift": 16,
                "w": 4,
                "x": 144,
                "y": 117
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2a43306d-5ea3-4b4c-93c8-9c15399b4f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 4,
                "shift": 16,
                "w": 10,
                "x": 204,
                "y": 94
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "fd82278c-3ee4-47a7-91ca-a1a4b062e039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 50,
                "y": 117
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}