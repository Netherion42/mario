{
    "id": "d6db88e8-a47f-4f75-a6d4-be819c2a5c34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2315c2fc-9b47-4a77-bf0c-6459d664e130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6db88e8-a47f-4f75-a6d4-be819c2a5c34",
            "compositeImage": {
                "id": "cbd2817f-0a85-4e5a-bef9-7b5a10a1edc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2315c2fc-9b47-4a77-bf0c-6459d664e130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a3b4a2-fc56-4ee7-b373-5ced314d0abd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2315c2fc-9b47-4a77-bf0c-6459d664e130",
                    "LayerId": "68220d19-5ae1-465f-9194-c827f68da1cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "68220d19-5ae1-465f-9194-c827f68da1cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6db88e8-a47f-4f75-a6d4-be819c2a5c34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 125
}