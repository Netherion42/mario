{
    "id": "f3f0de06-7fe1-4bfd-acdb-700258d1150f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu",
    "eventList": [
        {
            "id": "5617edf1-508b-4d0a-b67e-4de2af88204b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3f0de06-7fe1-4bfd-acdb-700258d1150f"
        },
        {
            "id": "a219585d-8cf5-4a08-be9a-841f727cf4c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3f0de06-7fe1-4bfd-acdb-700258d1150f"
        },
        {
            "id": "395ffff3-e181-4b0a-8714-f02a10026467",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3f0de06-7fe1-4bfd-acdb-700258d1150f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2360e10c-5f6a-4499-b7a9-94d4feea72d6",
    "visible": true
}