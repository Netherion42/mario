{
    "id": "3e806632-3e12-4d78-ae1d-7ac329a2c0f6",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "af1f6332-c058-4263-8df8-472e1bda0e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 14,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "62bc2a6d-7c71-4fcb-838a-1eab1cdeea9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 5,
                "x": 9,
                "y": 66
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "39ad989e-b5a4-463b-9cc8-e830eb5b1b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 133,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c1b8bc2c-9b5e-45ce-967c-13f80b084492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d991103c-0b45-492c-89da-93fdf65f235e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 34
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "b742c393-3f41-494d-899f-704be8166664",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 34
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "436ae33d-2b95-4c5e-883c-04c9e45723d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 34
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "be51899e-5d95-4dd3-b86d-f6d100559295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 4,
                "x": 23,
                "y": 66
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c5c26a83-96aa-4e01-b358-3ac43bd29e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 212,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "63a8c8d2-4e36-42f3-abf8-8972baa4421c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 6,
                "x": 229,
                "y": 50
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b744431a-2341-4056-a764-4d433f05ceb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 163,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "60a616e5-e9a0-41e0-bd80-2aadbe3c62a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 153,
                "y": 50
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "98b7748b-7b27-4241-809a-40ddef38edb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fa155513-e2cc-465e-bd24-f791d4a37368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 143,
                "y": 50
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9909118c-b79b-4f26-8df1-7d4ccf1d6b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 29,
                "y": 66
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "09e6d11e-a0c8-4a6c-9c26-11116b60243e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 34
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "77052a9f-d0b7-49d9-b0a0-932dc58a0255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 34
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1257e04a-de50-4e6b-bc5b-8f2d6677c6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 59,
                "y": 50
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ba798be9-1bd8-40e7-9c57-17939778e60c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ad9c86cd-8e0f-4a42-a399-b5289d78a19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 34
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9ebe8f44-2461-4928-bb7a-b605e6ab104c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9e9867ea-3bca-4b33-ab30-bbbd6a602c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 18
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "69adc160-a89e-4907-8944-bfbf7320202c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8bfde14d-557a-4785-a3a1-d5ebeb97878b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "119640a5-af8e-44c6-9c12-5fdd1285cba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 34
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "06b13334-33b2-48d1-ab73-ec6e48bc493b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 34
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d0b56f25-bbc8-42c7-8ad6-a3b90e87a07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 34,
                "y": 66
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "10ca8059-568b-4fb1-b5b4-8d343aa2a17f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 5,
                "x": 16,
                "y": 66
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a741e273-22d2-4873-832e-77cc138cfd8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 193,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5fad6aee-6b25-474c-9f6a-547fccb5718c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 183,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "42edb8a0-2e7b-46f7-b6e7-a399bf9d2cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 173,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a7dc5247-b75e-4087-89dd-f390e085f7cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 34
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c06c1204-ff35-4bda-bf09-83dbd36b9253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 34
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "dc33804a-da58-4862-b138-634b455ebf09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 34
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0a81e860-80f2-4c7e-b52c-cae554076646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 182,
                "y": 34
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "025b4a3a-513b-4528-b2c5-5374c2517b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 34
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7927c16c-16dc-46f0-90a8-7fb42c438590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 206,
                "y": 34
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "01f78720-8253-4f65-9397-c8e9f4c75b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 218,
                "y": 34
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "971df37d-32ec-4adc-a813-6389d96044ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 34
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "88547a56-7162-4563-8493-cc1faba82e90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 242,
                "y": 34
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "554d5491-d8d1-4af7-b96b-f1620fe196fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 34
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c83ad240-9ff2-4abb-8796-6269181a29f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 70,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "380ff642-46de-4f68-9461-ba2e4f236d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 218,
                "y": 18
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "142f75fc-5345-4853-9189-b95b5f1fe7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 206,
                "y": 18
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b2653d84-5237-4726-b150-9db25c4a9781",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "65602ce8-c824-40c0-87b3-d19478fac1da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a5bf9062-4e72-45ef-9af9-08c456f5e036",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "76ebc003-f507-4c08-bdf3-fc9e70ccf297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 230,
                "y": 18
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "45a0ff93-03aa-456c-bfd1-2c26897879a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7c40b02e-0bde-4aba-b969-ca780620f14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "de4936f6-6b7a-480d-8de2-ec3904c8d13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c29bf1d3-4705-479d-9a61-c780b8ea1cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3e6a04b4-67d7-4c82-9153-adc1d9d346c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 92,
                "y": 50
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2a7c5c22-8809-48bb-9183-bddac644acad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e3e714e4-6f41-469e-9667-a6311cda8c48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "0bfca606-ce5f-437d-be5d-53503734cccb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3b430b47-5941-40a4-b8db-9beb9cef5d5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "04cd65ed-793a-4a64-8182-7b2e577cfd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 81,
                "y": 50
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4d2d3dcd-1a8e-4139-8afa-52baad40ddd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ffc93463-edd0-4d5e-a922-a92730224e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 6,
                "x": 237,
                "y": 50
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "eddffe39-af6f-4e15-ae67-2a9b3aa3643d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c0fc3212-f31e-4ff4-90b7-feeda76e2aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 203,
                "y": 50
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a545e6c1-9242-475c-9a0c-1849ab5dbf13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 103,
                "y": 50
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ae066d39-790a-4a07-85ab-6bf34ef961ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c58566ff-d214-466d-a7c6-6e6d1dcaf98e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 14,
                "offset": 5,
                "shift": 11,
                "w": 5,
                "x": 245,
                "y": 50
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c09a1aad-34b8-415c-b5b3-7cc1e32a34f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7d2dfe1a-dac3-4afa-aa4f-4f93e5320fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "26ee716d-553e-4037-98ef-5356d9b0b7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ec8d3b33-1519-445f-9973-ce93a91c25cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a998848e-0e61-40bb-a791-6946e0ad1324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "93b11ebb-c061-4cc7-9ab7-ed5e1ebcf9a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4f8a6ed0-aebf-44b6-99cf-27117ca28953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 194,
                "y": 18
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "67233394-e72e-4adf-80e7-8bfa377dfc22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 182,
                "y": 18
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f3fd987e-e1d9-41b9-b93a-aa2037d1ad4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 37,
                "y": 50
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "177178c3-06db-4ae8-84f8-563d7e2d34c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 18
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6be087c1-a86e-402e-9399-22b97743db3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 158,
                "y": 18
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1f46cdc1-6ac4-40f8-829d-f53bc85552b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 146,
                "y": 18
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0a5f815f-1db5-4c89-923b-1da0f393107c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 134,
                "y": 18
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "93c7da42-f0d2-436b-b856-10c1ca204885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 122,
                "y": 18
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "19a7904b-4a94-4478-bcd2-f754b968e66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 18
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5e319f86-1249-42ce-9c3c-4a4bf3cabaf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 18
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dd1979cc-1799-461f-80a8-417854b4aed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 18
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "52a0e01a-9029-4795-8f5f-6e160d6e11fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 18
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ca13aec4-e4ac-4e07-9711-95e62f498833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 18
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "13bad5b4-4f89-4938-850b-e16f05099309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 26,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9b6d7d6a-0094-43a4-9508-5d8aadf5e29b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 18
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f83f3c00-e9ad-4e44-ab8a-af8215ab5e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 18
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0159f506-7e59-44e8-8604-48786ae6426a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 18
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "935df974-9b0c-468c-b65b-91422d6f3ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 18
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d5b6bd36-c490-48b3-b1bc-c2c961181545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 48,
                "y": 50
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a8d2f906-9f32-459f-8c35-01833c2f7786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 18
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6ed12267-042d-40fb-a6af-dec9c6d8fbc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 113,
                "y": 50
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4d52dfd7-8ea5-438d-b1ed-ce3c399a7cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 14,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 39,
                "y": 66
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3d6793a6-5d85-44ed-b6db-8b32d6d29a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 14,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5123f019-2d51-433a-95e1-e314162ff70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 221,
                "y": 50
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}