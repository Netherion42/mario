{
    "id": "97950fb9-a882-4438-b509-65ba68d5eae3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "suelo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 254,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd97670f-3cd1-460b-b800-e6f9e5793474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97950fb9-a882-4438-b509-65ba68d5eae3",
            "compositeImage": {
                "id": "5c62d45a-fe56-4dd5-9e53-21601e3662fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd97670f-3cd1-460b-b800-e6f9e5793474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28f9f6e2-a872-4d00-b30f-d40726777deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd97670f-3cd1-460b-b800-e6f9e5793474",
                    "LayerId": "89dd3686-f17f-4f19-80f4-08620f24606d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "89dd3686-f17f-4f19-80f4-08620f24606d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97950fb9-a882-4438-b509-65ba68d5eae3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 255,
    "xorig": 127,
    "yorig": 7
}