{
    "id": "6c4b3dc7-d9ca-426e-9cc9-e0c430a724dc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "cebbac76-9ad0-4cf5-b5a6-dd1dba21ba8e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "2425b134-50fa-4e83-9fa6-690cd6a0232d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 0
        },
        {
            "id": "38190a0b-661a-48f7-aae4-4ed81df77fc2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 15
        },
        {
            "id": "e1747154-2b70-4f35-b0e9-0e0d92ac567a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 15
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97950fb9-a882-4438-b509-65ba68d5eae3",
    "visible": true
}