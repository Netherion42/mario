{
    "id": "2360e10c-5f6a-4499-b7a9-94d4feea72d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "selector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05c02fe9-b4cc-4df2-9d64-b82be7f7d83e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2360e10c-5f6a-4499-b7a9-94d4feea72d6",
            "compositeImage": {
                "id": "167c3901-226e-40cb-b3f4-8207d9b532aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05c02fe9-b4cc-4df2-9d64-b82be7f7d83e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa02472e-6b00-4767-a03b-1201a1b3118c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05c02fe9-b4cc-4df2-9d64-b82be7f7d83e",
                    "LayerId": "5422b4a3-747b-4cf6-aee9-82d704cbcda9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "5422b4a3-747b-4cf6-aee9-82d704cbcda9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2360e10c-5f6a-4499-b7a9-94d4feea72d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}