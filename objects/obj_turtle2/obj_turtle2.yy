{
    "id": "c9e02100-b0f0-47f0-bc88-67077db61030",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_turtle2",
    "eventList": [
        {
            "id": "b5ca3fdf-5ac2-4094-8a4b-b89488dc61ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "e9e8b669-f34d-4151-830c-d0c8991489ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "47360870-c5a0-4211-8a13-5159da3c2f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5381da24-7973-4a3e-ad22-506ffc6bfe01",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "e8ef5d35-517a-49f9-990a-259989c5df0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "ef90ef57-a270-4d6d-8713-e7eec36a576c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "a3038a2b-632c-45d5-8ce7-19f8e1ada1d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b8f4cd2e-bf90-4806-b816-de6c56071517",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        },
        {
            "id": "65022c97-2670-457d-be7e-2bfc610292a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c9e02100-b0f0-47f0-bc88-67077db61030"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "708e995b-ca61-42ef-b50e-bd5100fc565f",
    "visible": true
}