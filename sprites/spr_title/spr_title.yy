{
    "id": "1845c18f-561d-410f-a7f4-17d8a53276f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71afc3ac-4615-4da6-9346-0acf7d310585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1845c18f-561d-410f-a7f4-17d8a53276f5",
            "compositeImage": {
                "id": "abd16451-0b67-4c44-8345-917164f0f327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71afc3ac-4615-4da6-9346-0acf7d310585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a919ee21-5260-4b71-a50a-312dfd991a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71afc3ac-4615-4da6-9346-0acf7d310585",
                    "LayerId": "cb3425b9-b571-4d41-9371-bf00e777dc37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "cb3425b9-b571-4d41-9371-bf00e777dc37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1845c18f-561d-410f-a7f4-17d8a53276f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 105,
    "yorig": 41
}