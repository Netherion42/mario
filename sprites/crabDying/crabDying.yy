{
    "id": "150982f9-624e-4823-bf5c-ea768422ba3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crabDying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eef1d952-3710-487b-81ce-065fc7998ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "150982f9-624e-4823-bf5c-ea768422ba3f",
            "compositeImage": {
                "id": "94c9724e-216a-49ad-8796-371c941ae5cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eef1d952-3710-487b-81ce-065fc7998ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde154a7-8896-4b0e-8717-9f0d91febd5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eef1d952-3710-487b-81ce-065fc7998ebd",
                    "LayerId": "f2300c00-6f24-46d1-b8dd-8ec6271e418f"
                }
            ]
        },
        {
            "id": "52b97f71-1049-47ba-8b63-0abc7b3a2356",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "150982f9-624e-4823-bf5c-ea768422ba3f",
            "compositeImage": {
                "id": "53d4ccac-f7b5-4cb7-9735-713f094b118f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b97f71-1049-47ba-8b63-0abc7b3a2356",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df428556-85e8-4fc7-8cc4-d00e531f4bc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b97f71-1049-47ba-8b63-0abc7b3a2356",
                    "LayerId": "f2300c00-6f24-46d1-b8dd-8ec6271e418f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "f2300c00-6f24-46d1-b8dd-8ec6271e418f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "150982f9-624e-4823-bf5c-ea768422ba3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}