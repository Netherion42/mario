{
    "id": "95f0bbba-9da1-42e9-8cc0-f786cb50f637",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_score",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a76150fd-18d2-4735-a54b-a824f903a7af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b719f9cd-e5b9-4e7e-9409-9052b007c38b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e59c812d-5f57-47ce-91bb-c25fa5c4e143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 119,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "06c0991a-0eaf-49a3-bb38-e7b32cec00b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "efbd81fc-0c59-497a-9e50-edf606547d24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "47dd7df7-7379-4e7a-b4f3-1cc4e01672c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bafe11cb-47e9-4fcc-a9d7-336557eab4d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 35
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "348c7cea-2a9f-4078-864b-a93e4fbe30c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 73,
                "y": 68
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "63926629-93ff-4b44-a87b-91ea600741cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 31,
                "y": 68
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "99f1298e-44fe-4be6-9b40-95eb109d8b2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 24,
                "y": 68
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0ea7680a-1a6e-4204-a428-35fe7ef9bfe9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 57
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2ca04208-d496-40f3-9167-14398b3bd8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9d44cf70-4e5b-4260-b0c1-37cc682b9960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 50,
                "y": 68
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cb33f9cc-bdfd-4310-9203-bbf736ea7f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 57
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "30300167-6759-42b6-8f19-3f65eee7c4b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 78,
                "y": 68
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "09fc6c23-2c73-4076-8b23-3ed284ce20ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 35
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "74a99501-045a-4082-9951-c5df9aada662",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0e0ea783-f6a2-4198-9b52-5469629eb961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 106,
                "y": 57
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "0647d9b8-d6e3-48ab-9729-a633470e99d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b24dd20b-73e4-47ef-9200-5eaa47d17127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 35
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9903d547-ab4b-4b2c-a4ad-384ad133e566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 35
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "fb3a3a1e-7071-4fc5-b231-870f8f9dd42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "183657da-ecd5-4b12-9ff7-fb90a9019976",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "5af39b5c-29c7-41dc-ae3c-3a21c9e1343e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "aa7a0c5a-ab9f-40b2-9e3d-ed46d434bb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "627a2367-efc8-40d1-bf50-671338998a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 35
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6778b786-e3f7-4ed7-a5e5-796f8c62d90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 68,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9b203387-5d1c-4ae6-9d82-89c46439506c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 56,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "11a483a5-814f-40ec-9207-1b5da65c241a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "01a25769-0bfc-4e2e-aacf-f1a212f89a79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 114,
                "y": 57
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ccefdd79-54fb-40e4-8d82-62762c8e50a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 98,
                "y": 57
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "423f8afb-5167-4339-9c9e-0afff752c681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3852a5c8-0865-4966-805d-dafd49ecbd04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9e71c53a-8ce1-43be-8df5-cdf95911c131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 46
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1aaec0a9-611e-460a-9923-1e18c7309648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 46
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "81426956-fffe-4a39-b15d-6060c8b984c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "eb762686-b317-4bc6-be5c-bc4e18c7b231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ab742960-e501-4c1d-9aee-637092fcf5ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 46
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f9970aef-5a05-48fd-804c-9e07bcbcb75e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "223ea02a-fa85-40d1-a8b0-ea03b9a4f11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 46
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4b9e593c-ff68-4dde-9cd3-9acdf934e2f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "63687e2c-23ae-406a-98ae-9d3d464f3108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 58,
                "y": 57
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "766ba1b2-406c-4897-84b0-f84a762b4e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8ae820ff-3b19-469f-88ca-6c629a294059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "4f9dd011-d661-414a-8aa5-07c5f693f9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 13
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d9f6cbe8-178c-45f8-901a-eefc2e07756d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 13
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "1f42dad3-64ba-4f93-b716-07e6288b0973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 13
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a8ff4487-748c-48b2-96eb-ca47751bdc65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 35
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b5d4a75a-8f73-4565-9d77-956fbb81d2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6aaa31eb-5c73-4e26-a264-77e86ddd89e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7cddc927-1a02-4e56-b9f6-735b80e207c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0b8ff768-8522-4f8e-917d-0bbaed6e528f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "72e4e5f2-c6ad-4b4c-846a-824cd05aac96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 57
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "3a70f54d-8311-4a8a-ae52-59b55903f280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 13
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "44017f30-eabe-4628-aed4-cd5513b920cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9eb1532b-940d-45ca-8ba8-a8a51ed9c586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7d40cc53-8628-4585-a18d-d06a311aa482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "80a9ab48-426c-4fd0-b12f-2ea61b34ef19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 57
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7aa302fe-732f-47a9-bba6-4027f36ab57b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8ab3ae02-d8f1-404b-9b22-7b0b4c3d0d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 17,
                "y": 68
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "182c7d79-ac22-4d9c-b53d-c43e6ed99bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8c6019ef-bf41-4372-be68-815c23dfac9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 10,
                "y": 68
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6289dd69-c4e8-42fb-9a91-188ec7f6aae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 82,
                "y": 57
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fa4e75db-f75f-4797-9bdb-61ee6b21e3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "391d7500-791d-4217-80d9-1245e760b892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 3,
                "shift": 7,
                "w": 4,
                "x": 44,
                "y": 68
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "0f4be7d4-9e90-427b-97b4-bac351e7a516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8f80a0ca-3d7c-497c-a681-09f6e0c68669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a6a02b7a-9f6f-441d-ab00-815f88ee6927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "11e64a72-c24e-401f-9b46-fe57e94be60a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 13
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2db8f32b-dc51-47df-82a0-2640ab18cfe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 13
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "35ae5822-da46-412b-a38b-20f122a6052a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 13
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c3faf52d-c7fd-4e4c-a11b-c472d6fcd344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a15dc99f-5105-4f83-8bfa-a3c864220d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b8d9f3c1-acba-453d-a48f-566101f2476a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 57
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9d0fb029-9323-48b7-a0c1-00689fea87f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5f241d88-881c-45e9-a31c-908b7d2a7d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "abcf6105-4a67-4706-b0c6-ff2dddfd87a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c7059460-d730-4d4e-95c4-b10b8b535713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "264fe447-c4e8-4eb9-9324-e25bc9bee8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a27736f8-f4e2-47b9-b220-e2a3a10973f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5669a5d7-5120-41ae-8e73-916ef9ef537c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "84593344-3fbf-40d1-b15a-eb1d1e91a761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b7562459-75b8-4fc0-a8f7-c587fb9f100d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "57ca49e3-b70e-4ecc-92c4-f47a7eecd54f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "27d942f5-b175-460b-a320-31e236f20c80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 57
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "78712941-4dd9-4433-837c-18862ab5741f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 13
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "515ec330-b0c4-4b54-88fc-24b99a0fc8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "db3b78a9-535b-4fd2-8543-ee849a894ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 13
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "a0b75f2d-aef2-447e-b912-76dd60b2614c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2a97c454-176c-4a70-8fdb-8b1b784a4bff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 57
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "bea6a672-bcc9-4044-a071-f00d7d467f5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 13
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "20848493-fdaa-4ec4-88e1-0c7298379ee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 57
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "61b31da8-e1c8-48b1-87b3-8e8f80ad61b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 83,
                "y": 68
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c7ade9d6-c84b-4453-97ad-3820d3a21789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 57
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "734e714a-e6f2-43f1-a1e9-cdc49d4e4536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 62,
                "y": 68
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 5,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}