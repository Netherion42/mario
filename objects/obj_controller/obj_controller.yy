{
    "id": "4d26a302-cf9b-4034-900f-60f895c9318f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_controller",
    "eventList": [
        {
            "id": "d7802ecb-49bf-4dda-b3eb-ff46fd18432e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        },
        {
            "id": "d7f55376-26c5-4320-929d-1670ec1b698c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        },
        {
            "id": "a0f1d64c-8f2d-4740-a06e-567d6c8f08f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        },
        {
            "id": "c8d87c36-397c-4fb0-b8f1-f5850dccfdea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        },
        {
            "id": "72aeab74-477f-4d33-88a1-5641b503ea7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        },
        {
            "id": "2f8a28f8-ed27-42b6-afa4-b00ffab03ae7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4d26a302-cf9b-4034-900f-60f895c9318f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "e67fe1b9-916b-4d0c-9cd0-99953ef5dfde",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "300ebe8e-aa9a-4074-8530-f4914d01469f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "5b270af9-6611-4ed1-b145-6ff856c6e567",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "bfffb4b4-90cf-4ca9-8408-3f61f5c713b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}