{
    "id": "a228d94b-ce6d-455a-b841-be771e370389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crabWalking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f020bdb-a1df-439f-b023-c4b41b50e6b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a228d94b-ce6d-455a-b841-be771e370389",
            "compositeImage": {
                "id": "352a766d-4b9f-4702-b8c9-d2f0e060e213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f020bdb-a1df-439f-b023-c4b41b50e6b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d558ddd1-19bf-42b9-bd06-8b0757084390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f020bdb-a1df-439f-b023-c4b41b50e6b9",
                    "LayerId": "d2f16be1-cd00-4c9e-ba79-97eb4f26e125"
                }
            ]
        },
        {
            "id": "9310f2bd-1227-48d8-ba58-39cba66001f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a228d94b-ce6d-455a-b841-be771e370389",
            "compositeImage": {
                "id": "1bbdc314-3106-46b9-a10c-a46e4e7b68be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9310f2bd-1227-48d8-ba58-39cba66001f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "492762cd-684e-4a47-9919-83875abddc78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9310f2bd-1227-48d8-ba58-39cba66001f7",
                    "LayerId": "d2f16be1-cd00-4c9e-ba79-97eb4f26e125"
                }
            ]
        },
        {
            "id": "f60258f4-131c-4e5c-afaf-84bb44bb97b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a228d94b-ce6d-455a-b841-be771e370389",
            "compositeImage": {
                "id": "0df66190-899f-46b0-9ff6-4d78a6c6165d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60258f4-131c-4e5c-afaf-84bb44bb97b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c6c5032-01eb-476a-b9b3-857c8037eec2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60258f4-131c-4e5c-afaf-84bb44bb97b5",
                    "LayerId": "d2f16be1-cd00-4c9e-ba79-97eb4f26e125"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d2f16be1-cd00-4c9e-ba79-97eb4f26e125",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a228d94b-ce6d-455a-b841-be771e370389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}