{
    "id": "61d6d276-ca4b-4afe-acc1-5a169448c5a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_hidden",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c993f101-184c-42a1-bb62-8b40740c4bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61d6d276-ca4b-4afe-acc1-5a169448c5a0",
            "compositeImage": {
                "id": "7ac3baf2-bd24-4088-9a64-9fd2658a2698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c993f101-184c-42a1-bb62-8b40740c4bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f31c1c9-8bec-4c07-9bda-ddeb8ac7ba82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c993f101-184c-42a1-bb62-8b40740c4bc4",
                    "LayerId": "4409dd70-a2e2-497e-b1b0-b38021cdc5ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "4409dd70-a2e2-497e-b1b0-b38021cdc5ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61d6d276-ca4b-4afe-acc1-5a169448c5a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}