/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 2BF425B9
/// @DnDArgument : "code" "// Mover enemigo$(13_10)$(13_10)moveEnemy();$(13_10)$(13_10)// Detectar colision con plataforma$(13_10)$(13_10)var plataforma = instance_place(x, y, obj_platform);$(13_10)$(13_10)if(plataforma != noone)$(13_10){$(13_10)	dead = 1;$(13_10)	$(13_10)	if(noCollision == 0)$(13_10)	{$(13_10)		noCollision = 1;$(13_10)		y -= 6;	$(13_10)		alarm[1] = 10;$(13_10)	}$(13_10)	$(13_10)	killTurtle();	$(13_10)}"
// Mover enemigo

moveEnemy();

// Detectar colision con plataforma

var plataforma = instance_place(x, y, obj_platform);

if(plataforma != noone)
{
	dead = 1;
	
	if(noCollision == 0)
	{
		noCollision = 1;
		y -= 6;	
		alarm[1] = 10;
	}
	
	killTurtle();	
}

/// @DnDAction : YoYo Games.Movement.Wrap_Room
/// @DnDVersion : 1
/// @DnDHash : 2F9BF51A
/// @DnDArgument : "ver" "0"
move_wrap(1, 0, 0);