{
    "id": "b8f4cd2e-bf90-4806-b816-de6c56071517",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floorDestroyer",
    "eventList": [
        {
            "id": "51b9ff11-ce27-478e-a4cc-9cf72d1d3297",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b8f4cd2e-bf90-4806-b816-de6c56071517"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "6666b4e2-08ba-4fb9-9e07-4563946ad911",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "73e8ee3f-3cf0-43cd-8a46-112c093b8d5f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 0
        },
        {
            "id": "ff8aa50f-5b11-44f3-aa64-038e7be9c3d3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 255,
            "y": 15
        },
        {
            "id": "05738819-6d41-4845-b6c9-b70bf52e60d0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 15
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "97950fb9-a882-4438-b509-65ba68d5eae3",
    "visible": true
}