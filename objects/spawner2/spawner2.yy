{
    "id": "22dd4c97-3ae1-4fe4-bd13-20d4c3be4f6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spawner2",
    "eventList": [
        {
            "id": "d01d0255-0a6a-43b6-a69d-cd80926b4f51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22dd4c97-3ae1-4fe4-bd13-20d4c3be4f6a"
        },
        {
            "id": "01bbabf5-dc22-48a0-8386-f642fb2cf16a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "22dd4c97-3ae1-4fe4-bd13-20d4c3be4f6a"
        },
        {
            "id": "7dc23605-bf6c-442f-8d38-7c3adda79e9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "22dd4c97-3ae1-4fe4-bd13-20d4c3be4f6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}