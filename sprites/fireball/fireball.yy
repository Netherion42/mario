{
    "id": "3e12ba45-30c2-4d97-8e38-f913daaad104",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fireball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38c7178a-e093-410f-8e00-b434cec269b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "72693e84-299e-4c47-820b-9073b3772050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38c7178a-e093-410f-8e00-b434cec269b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6264a2-c3bc-4be1-883e-0fd9997c5743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38c7178a-e093-410f-8e00-b434cec269b4",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "10102e83-15e1-4846-bc4e-d3320d112ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "9b4fbca9-81db-41fa-8027-f739d479bdf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10102e83-15e1-4846-bc4e-d3320d112ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03c1dd51-7d3a-4595-b457-b42226878fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10102e83-15e1-4846-bc4e-d3320d112ae3",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "271b924c-0998-4182-8cb4-5bdee8632391",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "b784b386-3536-447a-a463-8d6e21871d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "271b924c-0998-4182-8cb4-5bdee8632391",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b77b4cbb-f8d3-40c3-ab35-7d1bd450cece",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "271b924c-0998-4182-8cb4-5bdee8632391",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "1565054f-337d-4996-a9e0-62155ae0c61c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "0e92a911-03e4-45a5-b198-5326ea145a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1565054f-337d-4996-a9e0-62155ae0c61c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9691e80-9dcb-466d-b36b-b9321c9f56c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1565054f-337d-4996-a9e0-62155ae0c61c",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "d072f383-34fc-493d-9a3a-8bc2d09a1969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "b8caaf1c-ba95-44cb-aaea-359ee9e95594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d072f383-34fc-493d-9a3a-8bc2d09a1969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "314ad93f-4341-4875-ab26-f1756e7c096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d072f383-34fc-493d-9a3a-8bc2d09a1969",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "fe4c7997-938f-4913-93be-04474bfaa1a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "1b60dee0-3631-4dfc-8b9e-d36ae28de930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe4c7997-938f-4913-93be-04474bfaa1a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce4df0c-e89a-4c04-b45c-d3691936d8ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe4c7997-938f-4913-93be-04474bfaa1a9",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "2390476f-5312-4a91-8788-29ec588d4995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "137a5a2d-53b5-4dca-a897-a6c951d07616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2390476f-5312-4a91-8788-29ec588d4995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ba977f-fb46-44bd-99c0-3f599b298213",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2390476f-5312-4a91-8788-29ec588d4995",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        },
        {
            "id": "87c6da5e-c4a9-48f4-86d6-bc216fbd772c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "compositeImage": {
                "id": "905696d3-2dda-49c9-b389-9ee6705cf3d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87c6da5e-c4a9-48f4-86d6-bc216fbd772c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea3e2ce2-6546-4526-b84b-332cbd144218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87c6da5e-c4a9-48f4-86d6-bc216fbd772c",
                    "LayerId": "36596c7e-a800-41ab-aae3-7bf3f9ac9945"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "36596c7e-a800-41ab-aae3-7bf3f9ac9945",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e12ba45-30c2-4d97-8e38-f913daaad104",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 7
}