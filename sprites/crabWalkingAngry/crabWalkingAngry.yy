{
    "id": "6b86b383-a7a7-4357-abef-a007d77eb968",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crabWalkingAngry",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e3e33da-c950-418c-b7a8-159d50447fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b86b383-a7a7-4357-abef-a007d77eb968",
            "compositeImage": {
                "id": "3a9ab6c1-4fab-429c-b073-53b7ed1dd68e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e3e33da-c950-418c-b7a8-159d50447fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4431c35-7895-426b-8993-5d21566db9b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e3e33da-c950-418c-b7a8-159d50447fef",
                    "LayerId": "1a344e64-bd42-4c91-900a-682508516122"
                }
            ]
        },
        {
            "id": "56dae6f4-0837-47d3-8584-26ace04f6fc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b86b383-a7a7-4357-abef-a007d77eb968",
            "compositeImage": {
                "id": "04f24c76-009e-49c3-8a0c-df576a445c86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56dae6f4-0837-47d3-8584-26ace04f6fc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da051f09-3914-4768-860d-ce2f64ddead8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56dae6f4-0837-47d3-8584-26ace04f6fc6",
                    "LayerId": "1a344e64-bd42-4c91-900a-682508516122"
                }
            ]
        },
        {
            "id": "1104b5cc-1db8-4819-b17a-2c9396392d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b86b383-a7a7-4357-abef-a007d77eb968",
            "compositeImage": {
                "id": "19098d3b-4b62-4a0b-98f8-8030647e39ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1104b5cc-1db8-4819-b17a-2c9396392d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e62826aa-900b-413a-8d6b-e843ed7e7476",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1104b5cc-1db8-4819-b17a-2c9396392d3a",
                    "LayerId": "1a344e64-bd42-4c91-900a-682508516122"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1a344e64-bd42-4c91-900a-682508516122",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b86b383-a7a7-4357-abef-a007d77eb968",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}