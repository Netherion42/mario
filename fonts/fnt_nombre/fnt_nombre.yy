{
    "id": "2a7bd1eb-8f74-432f-b2da-0ba64ffdb8dd",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_nombre",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Emulogic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "57f0080d-b342-47ad-9b74-f781a0b6c09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "46d48fa5-2d52-4994-abaa-9afcaa29a461",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "64c71c88-d35d-4260-9a62-7e5aacc1e1a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 71,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a37a455d-386d-4f46-b49f-d0bfe015ec8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "2da3f5ba-162b-4fbc-aedf-4042f0ea7f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e76b5a44-ee65-4774-bf9f-b005dddf19f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6a0fecf5-2b52-45d1-9ead-4da1732ce135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 38
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cc53c18c-c7ee-483a-8f83-e5258665c98b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 2,
                "x": 58,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "79903f4a-e1ad-4f69-9212-26c8bf477467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 8,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "36a494d1-3389-4e4f-a645-a886bbbbdf5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "76d60ac1-17f7-438e-b02f-a3872753817c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 120,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e64a1ffe-7067-4e4c-977e-cfd240e3ef65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d6d24ad1-d4b1-4bdf-a90a-f91607387942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 36,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "57df902a-5553-4058-99d2-c09883d07308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 113,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b03e89e1-2549-46bf-8599-4f3b7cd56ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 46,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9f07d30b-a144-4d95-b61f-926b7d5238bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 38
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "632ba408-75dc-401f-8b75-eaeb674e3ace",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 38
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8ff8e434-7586-40cf-b649-5eb52c725054",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 18,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2fdc9b21-8f9f-4469-b074-225f2b44a756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 38
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "933e3924-5ddb-4362-8967-5cbecf0d1401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 38
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "526f6968-d900-4356-a65a-ca5462bd1c99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "db272c3e-3237-4712-8aeb-015b1a3188a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d4e9fa18-95c0-4155-87a7-eb24fe89729f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "26cf470c-1c79-4c48-9117-638497e88911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 38
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7d79bff3-93b6-4f91-91df-9967719d442e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 38
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff0b8ba8-e019-4cea-ad9f-7056380815a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 38
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "47b96021-3876-4655-b7ec-ae79ae7cb6b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c0e9b858-15cf-4450-b728-f89c6d623c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 3,
                "x": 31,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b54dd5e8-0e96-4b38-be91-cc32216ed08d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6e4f3e6a-ed15-48f1-994f-1656681ffe2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 85,
                "y": 62
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "67c04667-4b1f-4e23-b3e2-46c3d5679754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 78,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "882fb2bb-f42e-4a6c-8026-0f8836429adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f1f9618f-8378-441a-bc67-879a98f56302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "89df5d0f-43e4-405a-8c36-f7590922fdfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2b838d94-b298-428d-8a3a-8f5b425d047b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7dd88e4a-e3c5-47b1-9729-28954b175372",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "43a69913-35dc-4cae-b01e-333b56d6e552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "48d2ca9f-f5e2-4d7d-a198-4c161d9b4a38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7b299e3f-db92-4c62-9831-afce58ab74ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f6170a48-64b4-4376-a53c-b09ec24159e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5faa9450-36b7-4dcf-bd27-699887c78e25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 38
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5c43f5aa-0648-4dd2-a8dd-9a08aa6cc196",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 26,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "079e4caa-31ac-4b62-88ae-1564ee922038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "21ca2bdb-3668-42a0-b283-2b32a233842d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b186d403-4a32-4b12-b82f-60ea313ce471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 14
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "109034aa-a5ce-4d37-8a90-09522f226682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 14
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fd6bef77-208c-400c-832f-8c25f22b0191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 14
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b15295dc-1b8c-4083-94d1-e17669f4f521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 38
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "659a0857-d5a7-4030-91cf-c4da39f0725a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 14
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e8b7c1fe-73e7-4170-8569-b6cca438ebcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7e6e3a7f-cbd1-4669-9a06-4b19e3475359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2d6e58db-b2cd-4385-98b1-3be72b8be6b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f12204ea-5e44-4bdf-95dd-5ee296399f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c143ff8b-f6a1-481b-8ce3-f7d6961dbdef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 14
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bbf90521-564a-4ddf-94d0-8490dcb9c2fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6d22a680-8fa4-4d36-ab9d-934b0fccc897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "45b637c2-136b-4b72-a903-e2821c5fdf7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a4f553a8-ece7-4326-8569-06c8fa17b4b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 34,
                "y": 62
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "145d18c2-4fc0-46a4-8b17-be326b9b9833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6e930f33-9055-4217-a409-39165326beb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fdc5be94-bdef-4331-aec4-53bd0a275d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bf1a9550-462a-404a-a0ca-288d61ce26e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 20,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b7c613a1-2fec-48cc-b04b-6a6d3c013d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 50,
                "y": 62
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d176cef9-855d-4bcd-8857-38981e4e6512",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7310b01a-d6b6-4726-a200-30c45339e969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 10,
                "offset": 4,
                "shift": 8,
                "w": 3,
                "x": 26,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "da487e05-5f1d-4562-80e4-32e487b5ffd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0a2e89a6-2340-4857-a559-8721bcb4f8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5d810e7f-a647-41f2-9d38-9f00059d9699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2dbc3521-3e35-43bc-93a4-28f76ebb6d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 14
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "29ddc12b-3402-4fbb-a12d-2c0d563b3fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 14
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "bbb5c7a4-c956-4daf-92b7-8207136757e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 14
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "aedbcd56-7ba3-4998-a231-e893660901d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 26
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9b6b45b6-5195-44a0-a626-d13a778ab20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "548efd18-17b9-41ca-860c-e9610c9828ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ace8b391-54fe-41ff-a048-748f2fc28144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "22334979-ef63-4754-aa6f-fdb116a9d034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9bce5877-8839-42f1-94cd-f924e14f0684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dfc3bef6-08a0-4ede-bf05-e647718d7333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "35ff0879-d55c-458e-ba99-1cc1515e080a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 47,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "643e996d-76f3-4711-8153-c2b9f9757bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 38,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d311189e-7565-4fde-bf8c-e732bd84d9ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 29,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6819c16c-a6de-46ae-9324-82f93aedd877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "71178a80-626b-4186-916f-8c8aace0a03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a5c1f63a-e243-4040-9642-5de6004cde57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0469b222-e2d0-4af9-b7df-ae4ffe8ac8f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 119,
                "y": 50
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a1f5b909-c9d0-448a-a896-d4e0d258862b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 110,
                "y": 14
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c8099d78-ddd4-448c-91ce-1ba12b525a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 101,
                "y": 14
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "14d86ac5-ed6d-4e83-aeb4-0065b3f7a7d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 92,
                "y": 14
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "41546a84-e73a-43fb-9004-5fd04aff56b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 14
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "03059408-697d-499d-b99f-08afc629716d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 62
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ce7f9e65-c352-4d82-bf3c-cb60576f9b8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 74,
                "y": 14
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "39bcac02-aea5-4048-94ad-384544504201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 57,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6d72adab-3b39-4100-879d-a1cb4eb36bce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 10,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 54,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "14c3283f-fdf6-42f2-806f-7fc0dc906f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 10,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 64,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a5166421-99b8-498e-a015-aa54703be3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 62
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}