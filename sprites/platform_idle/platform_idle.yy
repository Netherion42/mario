{
    "id": "73224090-8f03-4177-80f2-3d13d2696b11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "platform_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "228e53dd-e60c-440d-b85e-a116d58eb0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73224090-8f03-4177-80f2-3d13d2696b11",
            "compositeImage": {
                "id": "085b848e-4d50-4683-a74a-dc39da3de388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228e53dd-e60c-440d-b85e-a116d58eb0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3587a919-e0c3-4519-84dc-6e6f25f1c5fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228e53dd-e60c-440d-b85e-a116d58eb0da",
                    "LayerId": "92c05e29-337f-4693-a224-13def6407ce2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "92c05e29-337f-4693-a224-13def6407ce2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73224090-8f03-4177-80f2-3d13d2696b11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 4
}