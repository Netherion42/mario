/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 28DD4D05
/// @DnDArgument : "code" "// Aqui iran todas las variables globales$(13_10)$(13_10)global.roomHeight = 250;$(13_10)global.roomWidth = 256;$(13_10)$(13_10)global.enemiesKilled = 0;$(13_10)global.maxEnemies = 4;$(13_10)global.enemiesSpawned = 0;$(13_10)global.playerHeight = 21;$(13_10)global.contadorVueltas = 0;$(13_10)$(13_10)spawning = false;$(13_10)levelComplete = false;$(13_10)$(13_10)volverMenu = false;$(13_10)$(13_10)randomize();"
// Aqui iran todas las variables globales

global.roomHeight = 250;
global.roomWidth = 256;

global.enemiesKilled = 0;
global.maxEnemies = 4;
global.enemiesSpawned = 0;
global.playerHeight = 21;
global.contadorVueltas = 0;

spawning = false;
levelComplete = false;

volverMenu = false;

randomize();