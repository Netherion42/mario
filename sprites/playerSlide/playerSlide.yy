{
    "id": "fa975508-a229-4926-83bb-f5fd76314252",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerSlide",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "852c94d7-230c-40c2-b999-efb0c6a9554a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa975508-a229-4926-83bb-f5fd76314252",
            "compositeImage": {
                "id": "187068b8-5b48-4723-aaef-0b4d7faf235d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "852c94d7-230c-40c2-b999-efb0c6a9554a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc04034-e08a-4efb-9ea0-bb782b67e629",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "852c94d7-230c-40c2-b999-efb0c6a9554a",
                    "LayerId": "958c78b6-757b-42e8-9163-7f08b9988f9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "958c78b6-757b-42e8-9163-7f08b9988f9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa975508-a229-4926-83bb-f5fd76314252",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}