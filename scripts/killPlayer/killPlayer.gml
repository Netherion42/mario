/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 68851409
/// @DnDArgument : "code" "// Colision con tortuga$(13_10)$(13_10)var tortuga = instance_place(x, y, obj_turtle2);$(13_10)if(tortuga != noone)$(13_10){$(13_10)	if(tortuga.dead == 1)$(13_10)	{$(13_10)		show_debug_message("muere tortuga");$(13_10)		// eliminar tortuga y sumar puntos$(13_10)		if(tortuga.dying == 0)$(13_10)		{$(13_10)			global.puntos += 200;	$(13_10)			global.enemiesKilled++;$(13_10)			audio_play_sound(snd_kick, 1, 0);$(13_10)		}$(13_10)		$(13_10)		tortuga.dead = 2;$(13_10)		tortuga.dying = 1;$(13_10)	}$(13_10)	else$(13_10)	{$(13_10)		if(alive && tortuga.dead == 0)$(13_10)		{			$(13_10)			alive = false;$(13_10)			alarm[0] = 30;$(13_10)			sprite_index = playerDeath;$(13_10)			audio_play_sound(snd_die,1,0);$(13_10)			global.vidas--;$(13_10)			alarm[1] = 200;$(13_10)			show_debug_message("Vidas restantes: " + string(global.vidas));	$(13_10)		}$(13_10)	}$(13_10)}$(13_10)$(13_10)// Colision con cangrejo$(13_10)$(13_10)var crab = instance_place(x, y, obj_crab);$(13_10)$(13_10)if(crab != noone)$(13_10){$(13_10)	$(13_10)	if(crab.dead == 1)$(13_10)	{$(13_10)		if(crab.dying == 0)$(13_10)		{$(13_10)			global.puntos += 200;	$(13_10)			global.enemiesKilled++;$(13_10)			audio_play_sound(snd_kick, 1, 0);$(13_10)		}$(13_10)		//crab.dead = 2;$(13_10)		crab.dying = 1;$(13_10)	}$(13_10)	else if(alive)$(13_10)	{$(13_10)		alive = false;$(13_10)		alarm[0] = 30;$(13_10)		sprite_index = playerDeath;$(13_10)		audio_play_sound(snd_die,1,0);$(13_10)		global.vidas--;$(13_10)		alarm[1] = 200;$(13_10)		show_debug_message("Vidas restantes: " + string(global.vidas));	$(13_10)	}$(13_10)}"
// Colision con tortuga

var tortuga = instance_place(x, y, obj_turtle2);
if(tortuga != noone)
{
	if(tortuga.dead == 1)
	{
		show_debug_message("muere tortuga");
		// eliminar tortuga y sumar puntos
		if(tortuga.dying == 0)
		{
			global.puntos += 200;	
			global.enemiesKilled++;
			audio_play_sound(snd_kick, 1, 0);
		}
		
		tortuga.dead = 2;
		tortuga.dying = 1;
	}
	else
	{
		if(alive && tortuga.dead == 0)
		{			
			alive = false;
			alarm[0] = 30;
			sprite_index = playerDeath;
			audio_play_sound(snd_die,1,0);
			global.vidas--;
			alarm[1] = 200;
			show_debug_message("Vidas restantes: " + string(global.vidas));	
		}
	}
}

// Colision con cangrejo

var crab = instance_place(x, y, obj_crab);

if(crab != noone)
{
	
	if(crab.dead == 1)
	{
		if(crab.dying == 0)
		{
			global.puntos += 200;	
			global.enemiesKilled++;
			audio_play_sound(snd_kick, 1, 0);
		}
		//crab.dead = 2;
		crab.dying = 1;
	}
	else if(alive)
	{
		alive = false;
		alarm[0] = 30;
		sprite_index = playerDeath;
		audio_play_sound(snd_die,1,0);
		global.vidas--;
		alarm[1] = 200;
		show_debug_message("Vidas restantes: " + string(global.vidas));	
	}
}