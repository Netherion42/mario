{
    "id": "acac4c78-2190-43e0-b46b-2bfaec46e029",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ceafb7c6-b69b-4732-abef-84ebbbd18e6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acac4c78-2190-43e0-b46b-2bfaec46e029",
            "compositeImage": {
                "id": "a12a19b2-afcd-4e96-99f3-a9cfa293e72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceafb7c6-b69b-4732-abef-84ebbbd18e6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc108ab-29f5-49f3-84f6-327588802846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceafb7c6-b69b-4732-abef-84ebbbd18e6a",
                    "LayerId": "ce6ed95e-ba9f-4d86-94eb-b7e85eba799a"
                }
            ]
        },
        {
            "id": "55ef69d4-7848-48df-89b7-52ceeea1ff82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acac4c78-2190-43e0-b46b-2bfaec46e029",
            "compositeImage": {
                "id": "694cba03-e5d7-40d5-920e-7bb978030b5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55ef69d4-7848-48df-89b7-52ceeea1ff82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dec4e56e-af4c-4262-b86a-a281851497fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55ef69d4-7848-48df-89b7-52ceeea1ff82",
                    "LayerId": "ce6ed95e-ba9f-4d86-94eb-b7e85eba799a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "ce6ed95e-ba9f-4d86-94eb-b7e85eba799a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acac4c78-2190-43e0-b46b-2bfaec46e029",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 5
}