{
    "id": "72f92023-b7f3-4f50-8353-672d52524215",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c749bf85-f269-47a8-870a-44cd349b89c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72f92023-b7f3-4f50-8353-672d52524215",
            "compositeImage": {
                "id": "2aa1cbf8-6d3e-4436-a5b3-bea24ce091a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c749bf85-f269-47a8-870a-44cd349b89c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba647db0-db26-491d-bd98-bcfc082fe44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c749bf85-f269-47a8-870a-44cd349b89c5",
                    "LayerId": "63bb3512-0eef-4b5c-9372-a1b00db082c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "63bb3512-0eef-4b5c-9372-a1b00db082c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72f92023-b7f3-4f50-8353-672d52524215",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 10
}