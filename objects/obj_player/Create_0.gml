/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 59C24C23
/// @DnDArgument : "code" "grav = 0.3;$(13_10)hsp = 0;$(13_10)vsp = 0;$(13_10) $(13_10)jumpspeed = 6;$(13_10)movespeed = 2;$(13_10) $(13_10)//Constants$(13_10)grounded = false;$(13_10)jumping = false;$(13_10) $(13_10)//Horizontal jump constants$(13_10)hsp_jump_constant_small = 1;$(13_10)hsp_jump_constant_big = 2;$(13_10)hsp_jump_applied = 0;$(13_10) $(13_10)//Horizontal key pressed count$(13_10)hkp_count = 0;$(13_10)hkp_count_small = 2;$(13_10)hkp_count_big = 5;$(13_10) $(13_10)//Init variables$(13_10)key_left = 0;$(13_10)key_right = 0;$(13_10)key_jump = false;$(13_10)$(13_10)alive = true;$(13_10)startDie = false;"
grav = 0.3;
hsp = 0;
vsp = 0;
 
jumpspeed = 6;
movespeed = 2;
 
//Constants
grounded = false;
jumping = false;
 
//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 2;
hsp_jump_applied = 0;
 
//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;
 
//Init variables
key_left = 0;
key_right = 0;
key_jump = false;

alive = true;
startDie = false;