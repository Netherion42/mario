{
    "id": "95d08ddf-8d7d-4325-8bee-7ab160cc38d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_platform",
    "eventList": [
        {
            "id": "172cc3b9-877c-452d-b9a9-6474f4c370bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95d08ddf-8d7d-4325-8bee-7ab160cc38d6"
        },
        {
            "id": "9234cabb-97ed-48fe-be97-46307e7cadd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95d08ddf-8d7d-4325-8bee-7ab160cc38d6"
        },
        {
            "id": "7d7434cc-c142-4c4f-9e00-f0277ed5fa7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "95d08ddf-8d7d-4325-8bee-7ab160cc38d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "73224090-8f03-4177-80f2-3d13d2696b11",
    "visible": true
}