{
    "id": "bbc5038c-3f15-40a6-a89e-497f807e55cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "10a01bc0-746a-47d4-9a26-57bfe714f553",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbc5038c-3f15-40a6-a89e-497f807e55cd"
        },
        {
            "id": "23e4e1be-fff3-46d3-85b7-7d32c0a686cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbc5038c-3f15-40a6-a89e-497f807e55cd"
        },
        {
            "id": "ef4c409c-d8aa-44f4-9813-1c821830c042",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbc5038c-3f15-40a6-a89e-497f807e55cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acac4c78-2190-43e0-b46b-2bfaec46e029",
    "visible": true
}