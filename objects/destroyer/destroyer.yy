{
    "id": "5381da24-7973-4a3e-ad22-506ffc6bfe01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "destroyer",
    "eventList": [
        
    ],
    "maskSpriteId": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}