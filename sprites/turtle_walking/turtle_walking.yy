{
    "id": "708e995b-ca61-42ef-b50e-bd5100fc565f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e2ca1d5-c1d3-44ec-a4a6-16ec9c368355",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "708e995b-ca61-42ef-b50e-bd5100fc565f",
            "compositeImage": {
                "id": "513d1450-c5ac-4efc-aa07-f99bc9c9b847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e2ca1d5-c1d3-44ec-a4a6-16ec9c368355",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b461f0cb-d745-40fe-aed8-f8e1ba4027b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e2ca1d5-c1d3-44ec-a4a6-16ec9c368355",
                    "LayerId": "9781291b-90db-40ee-a95b-fbddafe89c8c"
                }
            ]
        },
        {
            "id": "6fe36718-3025-4ddf-9226-65b0b3ec89a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "708e995b-ca61-42ef-b50e-bd5100fc565f",
            "compositeImage": {
                "id": "02a9c693-4b7f-4fd1-bd5e-973181fb5d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fe36718-3025-4ddf-9226-65b0b3ec89a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80675ee-b99a-47b7-a3f5-fa107f7bb66d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fe36718-3025-4ddf-9226-65b0b3ec89a1",
                    "LayerId": "9781291b-90db-40ee-a95b-fbddafe89c8c"
                }
            ]
        },
        {
            "id": "267a39f5-d3f8-4182-95c2-78403112f1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "708e995b-ca61-42ef-b50e-bd5100fc565f",
            "compositeImage": {
                "id": "35d223a9-3962-41c4-b9f4-da6f48cd716e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267a39f5-d3f8-4182-95c2-78403112f1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5d4e7e-52b4-4b4f-93f5-d9884c53d844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267a39f5-d3f8-4182-95c2-78403112f1db",
                    "LayerId": "9781291b-90db-40ee-a95b-fbddafe89c8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9781291b-90db-40ee-a95b-fbddafe89c8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "708e995b-ca61-42ef-b50e-bd5100fc565f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}