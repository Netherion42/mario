{
    "id": "30ee8e2f-8479-4af7-a952-c01e019bb448",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56c7f522-f3f3-4180-9c84-0ccad8d0bfec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30ee8e2f-8479-4af7-a952-c01e019bb448",
            "compositeImage": {
                "id": "c62e1e84-bb62-4aca-abfa-1e28b746bb73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56c7f522-f3f3-4180-9c84-0ccad8d0bfec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e80e8bc7-5da9-49fe-ada2-1f8530774502",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56c7f522-f3f3-4180-9c84-0ccad8d0bfec",
                    "LayerId": "a6a3243e-cb71-43c5-9729-6440b6260636"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "a6a3243e-cb71-43c5-9729-6440b6260636",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30ee8e2f-8479-4af7-a952-c01e019bb448",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 11
}