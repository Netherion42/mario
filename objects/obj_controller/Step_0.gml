/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 42B001D0
/// @DnDArgument : "code" "// Detectar si hay jugador y, si no, spawnearlo$(13_10)$(13_10)if(!instance_exists(obj_player) && !spawning)$(13_10){$(13_10)	alarm[0] = 60;$(13_10)	spawning = true;$(13_10)}$(13_10)$(13_10)// Comprobar si se ha matado a todos los enemigos$(13_10)$(13_10)if(global.enemiesKilled == global.maxEnemies && levelComplete == false)$(13_10){$(13_10)	levelComplete = true;$(13_10)	alarm[2] = 100; // Redirigir a nuevo nivel$(13_10)}$(13_10)"
// Detectar si hay jugador y, si no, spawnearlo

if(!instance_exists(obj_player) && !spawning)
{
	alarm[0] = 60;
	spawning = true;
}

// Comprobar si se ha matado a todos los enemigos

if(global.enemiesKilled == global.maxEnemies && levelComplete == false)
{
	levelComplete = true;
	alarm[2] = 100; // Redirigir a nuevo nivel
}