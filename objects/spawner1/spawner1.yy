{
    "id": "0e03f514-d9fd-48a5-90bd-ce80dd47f10d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spawner1",
    "eventList": [
        {
            "id": "ac583de2-6a70-4cfb-8db6-7b64b7b8ebed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e03f514-d9fd-48a5-90bd-ce80dd47f10d"
        },
        {
            "id": "ebd1cf35-6bd4-4bf9-9940-8ad15ca276ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0e03f514-d9fd-48a5-90bd-ce80dd47f10d"
        },
        {
            "id": "e0ee917b-32af-4432-86f0-0f2de25b24d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "0e03f514-d9fd-48a5-90bd-ce80dd47f10d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}