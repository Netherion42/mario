{
    "id": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "65daea6f-7c4c-4ace-a6ea-63416949c9ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1"
        },
        {
            "id": "de5bd2bd-3346-4426-bdf7-af187e3ecd62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1"
        },
        {
            "id": "a4698a58-f1ee-4bd7-a9da-4679b091c938",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1"
        },
        {
            "id": "8ec1fc26-968b-4309-aaa0-c04fd84baaab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1"
        },
        {
            "id": "30c0d129-5314-47ed-ab1d-01c109901def",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "94d79bee-bb61-43a5-b6ee-dc5cf95183a1"
        }
    ],
    "maskSpriteId": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "be0d04b6-3bf9-47ab-9038-9dfc2e3d1232",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8adf0102-f22d-4c32-9f39-643096d6b454",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "f758d589-0732-4b11-bd6b-b0ddc2d2789b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 21
        },
        {
            "id": "ee85acb2-67ca-4d5f-9714-04f5d9b72117",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 21
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "845072a8-c3cd-46b7-a0a0-7b30988bf681",
    "visible": true
}