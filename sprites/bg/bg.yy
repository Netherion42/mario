{
    "id": "a2f715ee-59cd-4c4b-b74a-1a6d48621587",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee8f04a0-8e39-441f-a6e6-bb44effce66c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2f715ee-59cd-4c4b-b74a-1a6d48621587",
            "compositeImage": {
                "id": "bdb23932-6af8-4f39-b00f-e8f80eca357d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8f04a0-8e39-441f-a6e6-bb44effce66c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34f2dbe7-11e0-48f5-b48a-a73201f695e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8f04a0-8e39-441f-a6e6-bb44effce66c",
                    "LayerId": "ecbde4e2-2c1c-4e89-95e2-8b26606c8b7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "ecbde4e2-2c1c-4e89-95e2-8b26606c8b7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2f715ee-59cd-4c4b-b74a-1a6d48621587",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 125
}