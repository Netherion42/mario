/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 4118883F
/// @DnDArgument : "code" "if(global.vidas == 0)$(13_10){$(13_10)	spawning = false;$(13_10)	draw_set_halign(fa_left);$(13_10)	draw_set_valign(fa_middle);$(13_10)	draw_set_font(fnt_gameover);$(13_10)	draw_set_color(c_white);$(13_10)	$(13_10)	if(room != Menu)$(13_10)	{$(13_10)		draw_text(room_width/4-10, room_height/2, "GAME OVER");$(13_10)	}$(13_10)	if(volverMenu = false)$(13_10)	{$(13_10)		volverMenu = true;	$(13_10)		alarm[1] = 180;$(13_10)	}$(13_10)}$(13_10)$(13_10)if(levelComplete = true)$(13_10){$(13_10)	draw_set_halign(fa_left);$(13_10)	draw_set_valign(fa_middle);$(13_10)	draw_set_font(fnt_gameover);$(13_10)	draw_set_color(c_white);$(13_10)	$(13_10)	draw_text(room_width/2 - 120, room_height/2, "LEVEL COMPLETED");$(13_10)}$(13_10)"
if(global.vidas == 0)
{
	spawning = false;
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(fnt_gameover);
	draw_set_color(c_white);
	
	if(room != Menu)
	{
		draw_text(room_width/4-10, room_height/2, "GAME OVER");
	}
	if(volverMenu = false)
	{
		volverMenu = true;	
		alarm[1] = 180;
	}
}

if(levelComplete = true)
{
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(fnt_gameover);
	draw_set_color(c_white);
	
	draw_text(room_width/2 - 120, room_height/2, "LEVEL COMPLETED");
}