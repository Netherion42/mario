{
    "id": "b8e0898b-f54f-47f2-80cb-2577b8e95530",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "title",
    "eventList": [
        {
            "id": "1ab11cb6-7471-4208-a26a-430fdd24935e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b8e0898b-f54f-47f2-80cb-2577b8e95530"
        },
        {
            "id": "7b8a0484-0a21-4475-9ac5-b24baa443b89",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b8e0898b-f54f-47f2-80cb-2577b8e95530"
        },
        {
            "id": "521fb3f1-306a-4563-adb2-9cbc05c48ebd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "b8e0898b-f54f-47f2-80cb-2577b8e95530"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1845c18f-561d-410f-a7f4-17d8a53276f5",
    "visible": true
}